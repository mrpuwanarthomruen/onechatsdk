package oneplatfrom.onechat.onechatsdk

import android.content.Context
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.response.*
import oneplatfrom.onechat.onechatsdk.services.common.RepositoryCallback
import oneplatfrom.onechat.onechatsdk.services.repositorys.ContactRepository

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */

class Contact(var context: Context) {

    var mContactRepository = ContactRepository()

    fun onUserInfo(
        tokenUser: String,
        callback: RepositoryCallback<UserInfoResponse>
    ) {
        mContactRepository.onUserInfo(tokenUser, object : RepositoryCallback<UserInfoResponse> {
            override fun onSuccess(result: UserInfoResponse?) {
                callback.onSuccess(result)
            }

            override fun onFailure(t: Throwable?) {
                callback.onFailure(t)
            }

        })
    }

    fun onAddFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<AcceptFriendResponse>
    ) {
        mContactRepository.onAddFriend(
            tokenFriend,
            tokenUser,
            object : RepositoryCallback<AcceptFriendResponse> {
                override fun onSuccess(result: AcceptFriendResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onAcceptFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<AcceptFriendResponse>
    ) {
        mContactRepository.onAcceptFriend(
            tokenFriend,
            tokenUser,
            object : RepositoryCallback<AcceptFriendResponse> {
                override fun onSuccess(result: AcceptFriendResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onCancelRequestFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mContactRepository.onCancelRequestFriend(
            tokenFriend,
            tokenUser,
            object : RepositoryCallback<BaseResponse> {
                override fun onSuccess(result: BaseResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onCancelSendRequestFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mContactRepository.onCancelSendRequestFriend(
            tokenFriend,
            tokenUser,
            object : RepositoryCallback<BaseResponse> {
                override fun onSuccess(result: BaseResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onGetListFriend(
        tokenUser: String,
        callback: RepositoryCallback<GetListFriendResponse>
    ) {
        mContactRepository.onGetListFriend(
            tokenUser,
            object : RepositoryCallback<GetListFriendResponse> {
                override fun onSuccess(result: GetListFriendResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onGetListRequestFriend(
        tokenUser: String,
        callback: RepositoryCallback<GetListRequestFriendResponse>
    ) {
        mContactRepository.onGetListRequestFriend(
            tokenUser,
            object : RepositoryCallback<GetListRequestFriendResponse> {
                override fun onSuccess(result: GetListRequestFriendResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onGetListPendingFriend(
        tokenUser: String,
        callback: RepositoryCallback<GetListRequestFriendResponse>
    ) {
        mContactRepository.onGetListPendingFriend(
            tokenUser,
            object : RepositoryCallback<GetListRequestFriendResponse> {
                override fun onSuccess(result: GetListRequestFriendResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onCreateGroup(
        tokenUser: String,
        groupName: String,
        listMember: ArrayList<String>,
        callback: RepositoryCallback<CreateGroupResponse>
    ) {
        mContactRepository.onCreateGroup(
            tokenUser, groupName, listMember,
            object : RepositoryCallback<CreateGroupResponse> {
                override fun onSuccess(result: CreateGroupResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onGetGroup(
        tokenUser: String,
        groupId: String,
        callback: RepositoryCallback<GetGroupResponse>
    ) {
        mContactRepository.onGetGroup(
            tokenUser, groupId,
            object : RepositoryCallback<GetGroupResponse> {
                override fun onSuccess(result: GetGroupResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onSetNameGroup(
        groupName: String,
        groupId: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mContactRepository.onSetNameGroup(
            groupName, groupId,
            object : RepositoryCallback<BaseResponse> {
                override fun onSuccess(result: BaseResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onInviteMemberGroup(
        tokenUser: String,
        groupId: String,
        listMember: ArrayList<String>,
        callback: RepositoryCallback<InviteMemberGroupResponse>
    ) {
        mContactRepository.onInviteMemberGroup(
            tokenUser, groupId, listMember,
            object : RepositoryCallback<InviteMemberGroupResponse> {
                override fun onSuccess(result: InviteMemberGroupResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onLeaveGroup(
        tokenUser: String,
        groupId: String,
        listMember: ArrayList<String>,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mContactRepository.onLeaveGroup(
            tokenUser, groupId, listMember,
            object : RepositoryCallback<BaseResponse> {
                override fun onSuccess(result: BaseResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onAcceptInviteGroup(
        tokenUser: String,
        groupId: String,
        callback: RepositoryCallback<GetGroupResponse>
    ) {
        mContactRepository.onAcceptInviteGroup(
            tokenUser, groupId,
            object : RepositoryCallback<GetGroupResponse> {
                override fun onSuccess(result: GetGroupResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onGetListGroup(
        tokenUser: String,
        callback: RepositoryCallback<GetListGroupResponse>
    ) {
        mContactRepository.onGetListGroup(
            tokenUser,
            object : RepositoryCallback<GetListGroupResponse> {
                override fun onSuccess(result: GetListGroupResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

    fun onGetListPendingGroup(
        tokenUser: String,
        callback: RepositoryCallback<GetListPendingGroupResponse>
    ) {
        mContactRepository.onGetListPendingGroup(
            tokenUser,
            object : RepositoryCallback<GetListPendingGroupResponse> {
                override fun onSuccess(result: GetListPendingGroupResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }

//    fun onGetListChat(
//        tokenUser: String,
//        callback: RepositoryCallback<ChatResponse>
//    ) {
//        mContactRepository.onGetListChat(
//            tokenUser,
//            object : RepositoryCallback<ChatResponse> {
//                override fun onSuccess(result: ChatResponse?) {
//                    callback.onSuccess(result)
//                }
//
//                override fun onFailure(t: Throwable?) {
//                    callback.onFailure(t)
//                }
//
//            })
//    }

    fun onGetRoom(
        tokenUser: String,
        callback: RepositoryCallback<ChatResponse>
    ) {
        mContactRepository.onGetRoom(
            tokenUser,
            object : RepositoryCallback<ChatResponse> {
                override fun onSuccess(result: ChatResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }

            })
    }
}