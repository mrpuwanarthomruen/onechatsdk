package oneplatfrom.onechat.onechatsdk

import android.content.Context
import android.util.Base64
import com.google.gson.Gson
import oneplatfrom.onechat.onechatsdk.model.authentication.request.AuthenticationRequest
import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import oneplatfrom.onechat.onechatsdk.services.common.RepositoryCallback
import oneplatfrom.onechat.onechatsdk.services.repositorys.AuthenticationRepository
import java.util.*

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */

class Authentication(var context: Context) {

    var authenticationRepository = AuthenticationRepository()

    fun onSignIn(
        username: String,
        password: String,
        callback: RepositoryCallback<AuthenticationResponse>
    ) {
        if (username == "" && password == "") return
        val request = AuthenticationRequest().apply {
            this.username = username
            this.password = password
        }

        val endcode = encodeAuthen(request)
        endcode?.let {
            authenticationRepository.signIn(it,
                object : RepositoryCallback<AuthenticationResponse> {
                    override fun onSuccess(result: AuthenticationResponse?) {
                        callback.onSuccess(result)
                    }

                    override fun onFailure(t: Throwable?) {
                        callback.onFailure(t)
                    }
                })
        }
    }

    private fun encodeAuthen(model: AuthenticationRequest): String? {
        val gs = Gson()
        val json = gs.toJson(model)
        val encodeValue = Base64.encodeToString(json.toByteArray(), Base64.DEFAULT)
        val output = StringBuilder(encodeValue)
        output.reverse()
        val uniqueId: String?
        uniqueId = UUID.randomUUID().toString()
        val newUid = uniqueId.substring(0, 7)
        return output.toString() + newUid
    }
}