package oneplatfrom.onechat.onechatsdk

import android.content.Context
import oneplatfrom.onechat.onechatsdk.services.repositorys.ContactRepository

/**
 * Created by puwanarT on 23/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
open class ManageCenter {

    var authenRepository: Authentication? = null
    var contactRepository: Contact? = null
    var messageRepository : Message? = null

    fun authentication(context : Context): Authentication? {
        if (authenRepository == null) {
            authenRepository = Authentication(context)
        }
        return authenRepository
    }

    fun contact(context: Context): Contact? {
        if (contactRepository == null) {
            contactRepository = Contact(context)
        }

        return contactRepository
    }

    fun message(context: Context): Message? {
        if (messageRepository == null) {
            messageRepository = Message(context)
        }

        return messageRepository
    }


}