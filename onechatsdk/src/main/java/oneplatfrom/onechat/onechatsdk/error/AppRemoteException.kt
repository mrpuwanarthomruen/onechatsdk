package oneplatfrom.onechat.onechatsdk.error

import android.text.TextUtils
import retrofit2.Response
import java.io.IOException

class AppRemoteException : Exception {
    constructor(message: String?) : super(message)

    companion object {
        @Throws(IOException::class)
        fun parse(response: Response<*>): AppRemoteException {
            var msg: String? = ""
            if (response.errorBody() != null) {
                msg = response.errorBody()!!.string()
            }
            if (TextUtils.isEmpty(msg)) {
                msg = if (response.raw().networkResponse() != null) {
                    response.raw().networkResponse()!!.message()
                } else {
                    response.raw().message()
                }
            }
            if (TextUtils.isEmpty(msg)) {
                msg = response.message()
            }
            if (TextUtils.isEmpty(msg)) {
                msg = response.toString()
            }
            return AppRemoteException(msg)
        }
    }
}