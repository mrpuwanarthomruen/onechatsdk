package oneplatfrom.onechat.onechatsdk.model.contact


import com.google.gson.annotations.SerializedName

data class Member(
    @SerializedName("is_active")
    var isActive: Boolean,
    @SerializedName("mute")
    var mute: Boolean,
    @SerializedName("pin")
    var pin: Boolean,
    @SerializedName("status")
    var status: String,
    @SerializedName("timestamp_in")
    var timestampIn: String,
    @SerializedName("timestamp_out")
    var timestampOut: String,
    @SerializedName("tokenuser")
    var tokenuser: String
)