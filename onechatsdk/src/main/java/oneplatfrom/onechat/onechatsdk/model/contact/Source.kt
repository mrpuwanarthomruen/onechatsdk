package oneplatfrom.onechat.onechatsdk.model.contact


import com.google.gson.annotations.SerializedName

data class Source(
    @SerializedName("name")
    var name: String,
    @SerializedName("relation")
    var relation: String,
    @SerializedName("state")
    var state: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("tokenuser")
    var tokenuser: String
)