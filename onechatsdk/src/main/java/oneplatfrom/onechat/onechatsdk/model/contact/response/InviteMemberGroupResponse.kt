package oneplatfrom.onechat.onechatsdk.model.contact.response


import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse

data class InviteMemberGroupResponse(
    @SerializedName("group_id")
    var groupId: String,
    @SerializedName("listmember")
    var listmember: List<String>,
    @SerializedName("owner")
    var owner: String
) : BaseResponse()