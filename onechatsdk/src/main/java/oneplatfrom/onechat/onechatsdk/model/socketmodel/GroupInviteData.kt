package oneplatfrom.onechat.onechatsdk.model.socketmodel

/**
 * Created by puwanarT on 26/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class GroupInviteData {
    var tokenUser : String = ""
    var tokenRoom : String? = ""
    var groupId : String? = ""
    var memberInGroup : ArrayList<String>? = null
    var invited  : ArrayList<String>? = null
    var message : String? = ""
}