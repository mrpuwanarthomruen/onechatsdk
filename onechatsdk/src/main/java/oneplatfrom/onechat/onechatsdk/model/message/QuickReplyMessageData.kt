package oneplatfrom.onechat.onechatsdk.model.message

import com.google.gson.annotations.SerializedName

data class QuickReplyMessageData (
    @SerializedName("_id")
    var id: String,
    @SerializedName("timestamp")
    var timestamp: String,
    @SerializedName("state")
    var state: String,
    @SerializedName("tokenroom")
    var tokenroom: String?,
    @SerializedName("message")
    var message: String?,
    @SerializedName("tokenuser")
    var tokenuser: String,
    @SerializedName("quick_reply")
    var quick_reply: List<QuickReplyMessage>
)