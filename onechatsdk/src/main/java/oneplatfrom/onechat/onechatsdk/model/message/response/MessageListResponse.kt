package oneplatfrom.onechat.onechatsdk.model.message.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.message.MessageData

/**
 * Created by puwanarT on 26/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class MessageListResponse : BaseResponse() {
    @SerializedName("data")
    var massageList : List<MessageData>? = null
}