package oneplatfrom.onechat.onechatsdk.model.socketmodel

/**
 * Created by puwanarT on 26/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class MessageData {
    var tokenMessage : String? = ""
    var tokenRoom : String? = ""
    var tokenUser : String? = ""
}