package oneplatfrom.onechat.onechatsdk.model.contact


import com.google.gson.annotations.SerializedName

data class GroupData(
    @SerializedName("_id")
    var id: String,
    @SerializedName("create_date")
    var createDate: String,
    @SerializedName("group_id")
    var groupId: String,
    @SerializedName("create_date")
    var create_date: String?,
    @SerializedName("type")
    var type: String?,
    @SerializedName("inviteby")
    var inviteby: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("picture")
    var picture: String,
    @SerializedName("tokenroom")
    var tokenroom: String,
    @SerializedName("member")
    var member: List<Member>
)