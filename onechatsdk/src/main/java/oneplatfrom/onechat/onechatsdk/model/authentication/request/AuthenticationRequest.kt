package oneplatfrom.onechat.onechatsdk.model.authentication.request

import com.google.gson.annotations.SerializedName

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class AuthenticationRequest {
    @SerializedName("username")
    var username: String? = ""
    @SerializedName("password")
    var password: String? = ""
}