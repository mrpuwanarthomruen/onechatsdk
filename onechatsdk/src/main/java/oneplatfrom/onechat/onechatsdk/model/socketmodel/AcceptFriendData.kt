package oneplatfrom.onechat.onechatsdk.model.socketmodel

/**
 * Created by puwanarT on 26/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class AcceptFriendData {
    var tokenUser : String? = ""
    var tokenFriend : String? = ""
    var tokenRoom : String? = ""
    var timestamp : String? = ""
}