package oneplatfrom.onechat.onechatsdk.model.message


import com.google.gson.annotations.SerializedName

data class Answer(
    @SerializedName("label")
    var label: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("payload")
    var payload: String,
    @SerializedName("type")
    var type: String
)