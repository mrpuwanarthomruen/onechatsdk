package oneplatfrom.onechat.onechatsdk.model.message

import com.google.gson.annotations.SerializedName

data class ListStickerMessageData (
    @SerializedName("_id")
    var id: String,
    @SerializedName("list_sticker")
    var listSticker: List<ListStickerMessage>,
    @SerializedName("create_by")
    var createBy: String,
    @SerializedName("path_title")
    var pathTitle: String,
    @SerializedName("token_sticker")
    var tokenSticker: String?,
    @SerializedName("timestamp")
    var timeStamp: String?,
    @SerializedName("title_name")
    var titleName: String,
    @SerializedName("pic_title")
    var picTitle: String,
    @SerializedName("pic_title2")
    var picTitle2: String
)