package oneplatfrom.onechat.onechatsdk.model.contact


import com.google.gson.annotations.SerializedName

data class MemberChat(
    @SerializedName("is_active")
    var isActive: String,
    @SerializedName("pin")
    var pin: String,
    @SerializedName("start_msg")
    var startMsg: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("timestamp_in")
    var timestampIn: String,
    @SerializedName("timestamp_out")
    var timestampOut: String,
    @SerializedName("tokenuser")
    var tokenuser: String
)