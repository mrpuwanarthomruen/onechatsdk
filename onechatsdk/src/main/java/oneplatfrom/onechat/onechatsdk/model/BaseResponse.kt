package oneplatfrom.onechat.onechatsdk.model

import com.google.gson.annotations.SerializedName

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
open class BaseResponse {
    @SerializedName("message")
    var messageResponse: String? = null

    @SerializedName("status")
    var statusResponse: String? = null

    open fun getFormattedResponseMessage(): String? {
        return "[$statusResponse] $messageResponse"
    }
}