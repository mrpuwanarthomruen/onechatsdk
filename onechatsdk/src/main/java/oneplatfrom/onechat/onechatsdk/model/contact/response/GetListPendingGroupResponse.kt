package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.GroupData

class GetListPendingGroupResponse : BaseResponse() {
    @SerializedName("listGroup")
    var listGroup: ArrayList<GroupData>? = null
}