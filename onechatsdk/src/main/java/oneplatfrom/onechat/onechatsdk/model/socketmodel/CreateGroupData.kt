package oneplatfrom.onechat.onechatsdk.model.socketmodel

/**
 * Created by puwanarT on 26/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class CreateGroupData {
    var listMember : ArrayList<String>? = null
    var groupId : String? = ""
    var fromTokenUser : String? = ""
}