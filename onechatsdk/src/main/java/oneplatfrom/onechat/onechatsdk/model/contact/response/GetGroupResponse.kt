package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.Member
import oneplatfrom.onechat.onechatsdk.model.contact.GroupData

class GetGroupResponse : BaseResponse() {

    @SerializedName("groupData")
    var GroupData: GroupData? = null
}