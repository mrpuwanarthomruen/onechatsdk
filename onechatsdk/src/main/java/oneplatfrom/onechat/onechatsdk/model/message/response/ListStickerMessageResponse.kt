package oneplatfrom.onechat.onechatsdk.model.message.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.message.ListStickerMessageData

class ListStickerMessageResponse : BaseResponse() {
    @SerializedName("data")
    var massageListSticker : List<ListStickerMessageData>? = null
}