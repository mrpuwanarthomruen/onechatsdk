package oneplatfrom.onechat.onechatsdk.model.message


import com.google.gson.annotations.SerializedName

data class ListStickerMessage(
    @SerializedName("id")
    var id: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("message2")
    var message2: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("s3_name")
    var s3Name: String
)