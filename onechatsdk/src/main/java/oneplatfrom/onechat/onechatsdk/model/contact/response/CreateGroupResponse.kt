package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.GroupData

class CreateGroupResponse : BaseResponse() {

    @SerializedName("groupData")
    var groupData: GroupData? = null

}