package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.ListRequestFriend

class GetListRequestFriendResponse : BaseResponse() {
    @SerializedName("listrequestfriend")
    var listRequestFriend: ArrayList<ListRequestFriend>? = null
}