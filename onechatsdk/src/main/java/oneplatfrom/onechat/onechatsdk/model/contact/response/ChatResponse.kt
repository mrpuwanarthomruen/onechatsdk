package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.ChatData

/**
 * Created by puwanarT on 26/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class ChatResponse  : BaseResponse(){
    @SerializedName("listChat")
    var listChat : ArrayList<ChatData>? = null
}