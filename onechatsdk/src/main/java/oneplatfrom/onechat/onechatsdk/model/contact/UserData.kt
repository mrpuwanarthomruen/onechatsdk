package oneplatfrom.onechat.onechatsdk.model.contact


import com.google.gson.annotations.SerializedName

data class UserData(
    @SerializedName("account_title_eng")
    var accountTitleEng: String,
    @SerializedName("account_title_th")
    var accountTitleTh: String,
    @SerializedName("bot_id")
    var botId: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("first_name_eng")
    var firstNameEng: String,
    @SerializedName("first_name_th")
    var firstNameTh: String,
    @SerializedName("id_card_num")
    var idCardNum: String,
    @SerializedName("id_card_type")
    var idCardType: String,
    @SerializedName("last_name_eng")
    var lastNameEng: String,
    @SerializedName("last_name_th")
    var lastNameTh: String,
    @SerializedName("loa")
    var loa: String,
    @SerializedName("nickname")
    var nickname: String,
    @SerializedName("phone")
    var phone: String,
    @SerializedName("profilepicture")
    var profilepicture: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("tokenuser")
    var tokenuser: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("userid_one")
    var useridOne: String,
    @SerializedName("username")
    var username: String
)