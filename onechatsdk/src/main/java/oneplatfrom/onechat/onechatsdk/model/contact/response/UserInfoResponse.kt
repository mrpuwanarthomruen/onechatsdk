package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.UserInfo

/**
 * Created by puwanarT on 18/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class UserInfoResponse : BaseResponse() {

    @SerializedName("userData")
    var user_data: UserInfo? = null
}