package oneplatfrom.onechat.onechatsdk.model.message.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.message.QuickReplyMessageData

class QuickReplyMessageResponse : BaseResponse() {
    @SerializedName("data")
    var quickReplyMessage : QuickReplyMessageData? =null
}