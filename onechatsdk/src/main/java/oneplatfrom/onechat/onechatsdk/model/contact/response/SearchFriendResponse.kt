package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.ListUser

class SearchFriendResponse:BaseResponse() {

    @SerializedName("listUser")
    var listUser:List<ListUser>? = null

    @SerializedName("listBot")
    var listBot:List<ListUser>?= null
}