package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.Source

class AcceptFriendResponse : BaseResponse() {
    @SerializedName("source")
    var source: Source? = null

    @SerializedName("datetime")
    internal var dateTime: String? = null

    @SerializedName("owner")
    internal var owner: String? = null

    @SerializedName("tokenroom")
    internal var tokenRoom: String? = null
}