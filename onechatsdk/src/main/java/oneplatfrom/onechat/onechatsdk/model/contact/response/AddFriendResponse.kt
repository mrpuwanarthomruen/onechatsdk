package oneplatfrom.onechat.onechatsdk.model.contact.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.ListUser
import oneplatfrom.onechat.onechatsdk.model.contact.Source

class AddFriendResponse:BaseResponse() {

    @SerializedName("owner")
    var owner:String? = null

    @SerializedName("source")
    var source:Source?= null

    @SerializedName("tokenroom")
    var tokenroom:String? = null
}