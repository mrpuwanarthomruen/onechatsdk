package oneplatfrom.onechat.onechatsdk.model.contact

import com.google.gson.annotations.SerializedName

class ListUser {

    @SerializedName("dataUser")
    var dataUser:UserData? = null

    @SerializedName("relation")
    var relation:String? = null

}