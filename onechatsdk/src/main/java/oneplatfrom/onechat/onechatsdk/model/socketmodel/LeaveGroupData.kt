package oneplatfrom.onechat.onechatsdk.model.socketmodel

/**
 * Created by puwanarT on 26/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class LeaveGroupData {
    var tokenUser : String? = ""
    var tokenRoom : String? = ""
    var message : String? = ""
    var listmember : ArrayList<String>? = null
    var listKicker : ArrayList<String>? = null
}