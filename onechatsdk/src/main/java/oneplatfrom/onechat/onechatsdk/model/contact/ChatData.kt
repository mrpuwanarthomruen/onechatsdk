package oneplatfrom.onechat.onechatsdk.model.contact


import com.google.gson.annotations.SerializedName

data class ChatData(
    @SerializedName("count_read")
    var countRead: String,
    @SerializedName("create_date")
    var createDate: String,
    @SerializedName("display_name")
    var displayName: String,
    @SerializedName("_id")
    var id: String,
    @SerializedName("is_active")
    var isActive: String,
    @SerializedName("member")
    var member: List<MemberChat>,
    @SerializedName("message")
    var message: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("nickname")
    var nickname: String,
    @SerializedName("picture")
    var picture: String,
    @SerializedName("pin")
    var pin: String,
    @SerializedName("sender_name")
    var senderName: String,
    @SerializedName("timestamp")
    var timestamp: String,
    @SerializedName("tokenroom")
    var tokenroom: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("typeofmessage")
    var typeofmessage: String
)