package oneplatfrom.onechat.onechatsdk.model.message


import com.google.gson.annotations.SerializedName

data class MessageData(
    @SerializedName("_id")
    var id: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("nickname")
    var nickname: String,
    @SerializedName("profilepicture")
    var profilepicture: String,
    @SerializedName("read")
    var read: List<String>,
    @SerializedName("receive")
    var receive: List<String>,
    @SerializedName("timestamp")
    var timestamp: String,
    @SerializedName("tokenroom")
    var tokenroom: String,
    @SerializedName("tokenuser")
    var tokenuser: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("typeofmessage")
    var typeofmessage: String,
    @SerializedName("file_name")
    var file_name: String

)