package oneplatfrom.onechat.onechatsdk.model.message


import com.google.gson.annotations.SerializedName

data class QuickReplyMessage(
    @SerializedName("label")
    var label: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("onechat_token")
    var onechatToken: String,
    @SerializedName("sign")
    var sign: String,
    @SerializedName("type")
    var type: String
)