package oneplatfrom.onechat.onechatsdk.model.message


import com.google.gson.annotations.SerializedName

data class AnswerMessageRequest(
    @SerializedName("answer")
    var answer: Answer,
    @SerializedName("qrp_id")
    var qrpId: String,
    @SerializedName("tokenroom")
    var tokenroom: String,
    @SerializedName("tokenuser")
    var tokenuser: String
)