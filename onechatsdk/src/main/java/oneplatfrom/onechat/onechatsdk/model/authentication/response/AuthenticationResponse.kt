package oneplatfrom.onechat.onechatsdk.model.authentication.response

import com.google.gson.annotations.SerializedName
import oneplatfrom.onechat.onechatsdk.model.BaseResponse

class AuthenticationResponse : BaseResponse() {
    inner class Profile {
        @SerializedName("account_title_eng")
        var account_title_eng: String? = null

        @SerializedName("account_title_th")
        var account_title_th: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("first_name_eng")
        var first_name_eng: String? = null

        @SerializedName("first_name_th")
        var first_name_th: String? = null

        @SerializedName("id_card_num")
        var id_card_num: String? = null

        @SerializedName("id_card_type")
        var id_card_type: String? = null

        @SerializedName("ip")
        var ip: String? = null

        @SerializedName("last_name_eng")
        var last_name_eng: String? = null

        @SerializedName("last_name_th")
        var last_name_th: String? = null

        @SerializedName("loa")
        var loa: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("nickname")
        var nickname: String? = null

        @SerializedName("phone")
        var phone: String? = null

        @SerializedName("profilepicture")
        var pictureProfile: String? = null

        @SerializedName("status")
        var status: String? = null

        @SerializedName("tokenservice")
        var tokenservice: String? = null

        @SerializedName("tokenuser")
        var tokenUser: String? = null

        @SerializedName("type")
        var type: String? = null

        @SerializedName("one_id")
        var oneId: String? = null

        @SerializedName("username")
        var username: String? = null

    }

    inner class Account {
        @SerializedName("profile")
        var profile: Profile? = null
    }

    inner class Business {
        @SerializedName("business_id")
        var business_id: String? = null

        @SerializedName("business_name")
        var business_name: String? = null

        @SerializedName("business_pic")
        var business_pic: String? = null

        @SerializedName("role_id")
        var role_id: String? = null

        @SerializedName("tax_id")
        var tax_id: String? = null

        @SerializedName("role_name")
        var role_name: String? = null

    }

    @SerializedName("account")
    var account: Account? = null

    @SerializedName("business")
    var businesses: List<Business>? = null
}