package oneplatfrom.onechat.onechatsdk

import android.content.Context
import android.util.Base64
import com.google.gson.Gson
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.authentication.request.AuthenticationRequest
import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import oneplatfrom.onechat.onechatsdk.model.message.Answer
import oneplatfrom.onechat.onechatsdk.model.message.AnswerMessageRequest
import oneplatfrom.onechat.onechatsdk.model.message.response.ListStickerMessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageListResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.QuickReplyMessageResponse
import oneplatfrom.onechat.onechatsdk.services.common.RepositoryCallback
import oneplatfrom.onechat.onechatsdk.services.repositorys.AuthenticationRepository
import oneplatfrom.onechat.onechatsdk.services.repositorys.MessageRepository
import java.util.*

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */

class Message(var context: Context) {

    var messageRepository = MessageRepository()

    fun onGetMessage(
        tokenMessage: String,
        tokenUser: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        messageRepository.onGetMessage(tokenMessage, tokenUser,
            object : RepositoryCallback<MessageResponse> {
                override fun onSuccess(result: MessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onGetMessageList(
        tokenUser: String,
        tokenRoom: String,
        timeIn: String,
        callback: RepositoryCallback<MessageListResponse>
    ) {
        messageRepository.onGetMessageList(tokenUser, tokenRoom, timeIn,
            object : RepositoryCallback<MessageListResponse> {
                override fun onSuccess(result: MessageListResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onPushTextMessage(
        tokenUser: String,
        tokenRoom: String,
        message: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        messageRepository.onPushTextMessage(tokenUser, tokenRoom, message,
            object : RepositoryCallback<MessageResponse> {
                override fun onSuccess(result: MessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onPushLocationMessage(
        tokenUser: String,
        tokenRoom: String,
        lat: String,
        long: String,
        address: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        messageRepository.onPushLocationMessage(tokenUser, tokenRoom, lat, long, address,
            object : RepositoryCallback<MessageResponse> {
                override fun onSuccess(result: MessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onGetQuickReplyMessage(
        tokenRoom: String,
        tokenUser: String,
        callback: RepositoryCallback<QuickReplyMessageResponse>
    ) {
        messageRepository.onGetQuickReplyMessage(tokenRoom, tokenUser,
            object : RepositoryCallback<QuickReplyMessageResponse> {
                override fun onSuccess(result: QuickReplyMessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onGetDisableQuickReply(
        tokenRoom: String,
        tokenUser: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        messageRepository.onGetDisableQuickReply(tokenRoom, tokenUser,
            object : RepositoryCallback<BaseResponse> {
                override fun onSuccess(result: BaseResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onPushStickerMessage(
        tokenRoom: String,
        tokenUser: String,
        message: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        messageRepository.onPushStickerMessage(tokenRoom, tokenUser, message,
            object : RepositoryCallback<MessageResponse> {
                override fun onSuccess(result: MessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onGetListSticker(
        callback: RepositoryCallback<ListStickerMessageResponse>
    ) {
        messageRepository.onGetListStickerMessage(
            object : RepositoryCallback<ListStickerMessageResponse> {
                override fun onSuccess(result: ListStickerMessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onPushFileMessage(
        tokenRoom: String,
        tokenUser: String,
        pathUri: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        messageRepository.onPushFileMessage(tokenRoom, tokenUser, pathUri,
            object : RepositoryCallback<MessageResponse> {
                override fun onSuccess(result: MessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }

    fun onPushAnswerMessage(
        request : AnswerMessageRequest,
        callback: RepositoryCallback<MessageResponse>
    ) {
        messageRepository.onPushAnswerMessage(request,
            object : RepositoryCallback<MessageResponse> {
                override fun onSuccess(result: MessageResponse?) {
                    callback.onSuccess(result)
                }

                override fun onFailure(t: Throwable?) {
                    callback.onFailure(t)
                }
            })
    }


}
