package oneplatfrom.onechat.onechatsdk.services.socket;

import java.security.KeyStore;
import java.security.cert.Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

/**
 * @author Melody Dev
 * @since September 18, 2017.
 */

public class AppSocketCertificate {

    private Certificate certificate;
    private KeyStore keyStore;
    private TrustManager[] trustManagers;
    private SSLContext sslContext;

    public AppSocketCertificate(Certificate certificate, KeyStore keyStore, TrustManager[] trustManagers, SSLContext sslContext) {
        this.certificate = certificate;
        this.keyStore = keyStore;
        this.trustManagers = trustManagers;
        this.sslContext = sslContext;
    }

    //region Getters and Setters

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public KeyStore getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(KeyStore keyStore) {
        this.keyStore = keyStore;
    }

    public TrustManager[] getTrustManagers() {
        return trustManagers;
    }

    public void setTrustManagers(TrustManager[] trustManagers) {
        this.trustManagers = trustManagers;
    }

    public SSLContext getSslContext() {
        return sslContext;
    }

    public void setSslContext(SSLContext sslContext) {
        this.sslContext = sslContext;
    }


    //endregion
}
