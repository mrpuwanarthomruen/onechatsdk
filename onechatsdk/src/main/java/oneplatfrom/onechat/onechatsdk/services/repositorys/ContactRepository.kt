package oneplatfrom.onechat.onechatsdk.services.repositorys

import android.text.TextUtils
import oneplatfrom.onechat.onechatsdk.error.AppRemoteException
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.response.*
import oneplatfrom.onechat.onechatsdk.services.ServiceBase
import oneplatfrom.onechat.onechatsdk.services.common.RepositoryCallback
import oneplatfrom.onechat.onechatsdk.services.datasource.ChatNetworkDataSource
import oneplatfrom.onechat.onechatsdk.services.datasource.ContactNetworkDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactRepository {

    private var mDataSource: ContactNetworkDataSource = ContactNetworkDataSource()
    private var mDataChat : ChatNetworkDataSource = ChatNetworkDataSource()

    fun onUserInfo(token: String, callback: RepositoryCallback<UserInfoResponse>) {
        mDataSource.onUserInfo(token).enqueue(object : Callback<UserInfoResponse> {
            override fun onResponse(
                call: Call<UserInfoResponse>,
                response: Response<UserInfoResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        val dao = response.body() ?: throw AppRemoteException.parse(response)

                        if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                            callback.onSuccess(dao)
                        } else {
                            throw AppRemoteException(dao.getFormattedResponseMessage())
                        }
                    } else {
                        throw AppRemoteException.parse(response)
                    }
                } catch (e: Exception) {
                    callback.onFailure(e)
                }
            }

            override fun onFailure(call: Call<UserInfoResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback.onFailure(t)
                }
            }
        })
    }

    fun onAddFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<AcceptFriendResponse>
    ) {
        mDataSource.onAddFriend(tokenFriend, tokenUser)
            .enqueue(object : Callback<AcceptFriendResponse> {
                override fun onResponse(
                    call: Call<AcceptFriendResponse>,
                    response: Response<AcceptFriendResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<AcceptFriendResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onAcceptFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<AcceptFriendResponse>
    ) {
        mDataSource.onAcceptFriend(tokenFriend, tokenUser)
            .enqueue(object : Callback<AcceptFriendResponse> {
                override fun onResponse(
                    call: Call<AcceptFriendResponse>,
                    response: Response<AcceptFriendResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<AcceptFriendResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onCancelRequestFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mDataSource.onCancelRequestFriend(tokenFriend, tokenUser)
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onCancelSendRequestFriend(
        tokenFriend: String,
        tokenUser: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mDataSource.onCancelSendRequestFriend(tokenFriend, tokenUser)
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetListFriend(
        tokenUser: String,
        callback: RepositoryCallback<GetListFriendResponse>
    ) {
        mDataSource.onGetListFriend(tokenUser).enqueue(object : Callback<GetListFriendResponse> {
            override fun onResponse(
                call: Call<GetListFriendResponse>,
                response: Response<GetListFriendResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        val dao = response.body() ?: throw AppRemoteException.parse(response)

                        if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                            callback.onSuccess(dao)
                        } else {
                            throw AppRemoteException(dao.getFormattedResponseMessage())
                        }
                    } else {
                        throw AppRemoteException.parse(response)
                    }
                } catch (e: Exception) {
                    callback.onFailure(e)
                }
            }

            override fun onFailure(call: Call<GetListFriendResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback.onFailure(t)
                }
            }
        })
    }

    fun onGetListRequestFriend(
        tokenUser: String,
        callback: RepositoryCallback<GetListRequestFriendResponse>
    ) {
        mDataSource.onGetListRequestFriend(tokenUser)
            .enqueue(object : Callback<GetListRequestFriendResponse> {
                override fun onResponse(
                    call: Call<GetListRequestFriendResponse>,
                    response: Response<GetListRequestFriendResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<GetListRequestFriendResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetListPendingFriend(
        tokenUser: String,
        callback: RepositoryCallback<GetListRequestFriendResponse>
    ) {
        mDataSource.onGetListPendingFriend(tokenUser)
            .enqueue(object : Callback<GetListRequestFriendResponse> {
                override fun onResponse(
                    call: Call<GetListRequestFriendResponse>,
                    response: Response<GetListRequestFriendResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<GetListRequestFriendResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onCreateGroup(
        tokenUser: String,
        groupName: String,
        listMember: ArrayList<String>,
        callback: RepositoryCallback<CreateGroupResponse>
    ) {
        mDataSource.onCreateGroup(tokenUser, groupName, listMember)
            .enqueue(object : Callback<CreateGroupResponse> {
                override fun onResponse(
                    call: Call<CreateGroupResponse>,
                    response: Response<CreateGroupResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<CreateGroupResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetGroup(
        tokenUser: String,
        groupId: String,
        callback: RepositoryCallback<GetGroupResponse>
    ) {
        mDataSource.onGetGroup(tokenUser, groupId).enqueue(object : Callback<GetGroupResponse> {
            override fun onResponse(
                call: Call<GetGroupResponse>,
                response: Response<GetGroupResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        val dao = response.body() ?: throw AppRemoteException.parse(response)

                        if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                            callback.onSuccess(dao)
                        } else {
                            throw AppRemoteException(dao.getFormattedResponseMessage())
                        }
                    } else {
                        throw AppRemoteException.parse(response)
                    }
                } catch (e: Exception) {
                    callback.onFailure(e)
                }
            }

            override fun onFailure(call: Call<GetGroupResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback.onFailure(t)
                }
            }
        })
    }

    fun onSetNameGroup(
        groupName: String,
        groupId: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mDataSource.onSetNameGroup(groupName, groupId).enqueue(object : Callback<BaseResponse> {
            override fun onResponse(
                call: Call<BaseResponse>,
                response: Response<BaseResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        val dao = response.body() ?: throw AppRemoteException.parse(response)

                        if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                            callback.onSuccess(dao)
                        } else {
                            throw AppRemoteException(dao.getFormattedResponseMessage())
                        }
                    } else {
                        throw AppRemoteException.parse(response)
                    }
                } catch (e: Exception) {
                    callback.onFailure(e)
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback.onFailure(t)
                }
            }
        })
    }

    fun onInviteMemberGroup(
        tokenUser: String,
        groupId: String,
        listMember: ArrayList<String>,
        callback: RepositoryCallback<InviteMemberGroupResponse>
    ) {
        mDataSource.onInviteMemberGroup(tokenUser, groupId, listMember)
            .enqueue(object : Callback<InviteMemberGroupResponse> {
                override fun onResponse(
                    call: Call<InviteMemberGroupResponse>,
                    response: Response<InviteMemberGroupResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<InviteMemberGroupResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onLeaveGroup(
        tokenUser: String,
        groupId: String,
        listMember: ArrayList<String>,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mDataSource.onLeaveGroup(tokenUser, groupId, listMember)
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onAcceptInviteGroup(
        tokenUser: String,
        groupId: String,
        callback: RepositoryCallback<GetGroupResponse>
    ) {
        mDataSource.onAcceptInviteGroup(tokenUser, groupId)
            .enqueue(object : Callback<GetGroupResponse> {
                override fun onResponse(
                    call: Call<GetGroupResponse>,
                    response: Response<GetGroupResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<GetGroupResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetListGroup(
        tokenUser: String,
        callback: RepositoryCallback<GetListGroupResponse>
    ) {
        mDataSource.onGetListGroup(tokenUser).enqueue(object : Callback<GetListGroupResponse> {
            override fun onResponse(
                call: Call<GetListGroupResponse>,
                response: Response<GetListGroupResponse>
            ) {
                try {
                    if (response.isSuccessful) {
                        val dao = response.body() ?: throw AppRemoteException.parse(response)

                        if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                            callback.onSuccess(dao)
                        } else {
                            throw AppRemoteException(dao.getFormattedResponseMessage())
                        }
                    } else {
                        throw AppRemoteException.parse(response)
                    }
                } catch (e: Exception) {
                    callback.onFailure(e)
                }
            }

            override fun onFailure(call: Call<GetListGroupResponse>, t: Throwable) {
                if (!call.isCanceled) {
                    callback.onFailure(t)
                }
            }
        })
    }

    fun onGetListPendingGroup(
        tokenUser: String,
        callback: RepositoryCallback<GetListPendingGroupResponse>
    ) {
        mDataSource.onGetListPendingGroup(tokenUser)
            .enqueue(object : Callback<GetListPendingGroupResponse> {
                override fun onResponse(
                    call: Call<GetListPendingGroupResponse>,
                    response: Response<GetListPendingGroupResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<GetListPendingGroupResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }

//    fun onGetListChat(
//        tokenUser: String,
//        callback: RepositoryCallback<ChatResponse>
//    ) {
//        mDataSource.onGetListChat(tokenUser)
//            .enqueue(object : Callback<ChatResponse> {
//                override fun onResponse(
//                    call: Call<ChatResponse>,
//                    response: Response<ChatResponse>
//                ) {
//                    try {
//                        if (response.isSuccessful) {
//                            val dao = response.body() ?: throw AppRemoteException.parse(response)
//
//                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
//                                callback.onSuccess(dao)
//                            } else {
//                                throw AppRemoteException(dao.getFormattedResponseMessage())
//                            }
//                        } else {
//                            throw AppRemoteException.parse(response)
//                        }
//                    } catch (e: Exception) {
//                        callback.onFailure(e)
//                    }
//                }
//
//                override fun onFailure(call: Call<ChatResponse>, t: Throwable) {
//                    if (!call.isCanceled) {
//                        callback.onFailure(t)
//                    }
//                }
//            })
//    }


    fun onGetRoom(
        tokenUser: String,
        callback: RepositoryCallback<ChatResponse>
    ) {
        mDataChat.onGetRoom(tokenUser)
            .enqueue(object : Callback<ChatResponse> {
                override fun onResponse(
                    call: Call<ChatResponse>,
                    response: Response<ChatResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<ChatResponse>, t: Throwable) {
                    if (!call.isCanceled) {
                        callback.onFailure(t)
                    }
                }
            })
    }


}