package oneplatfrom.onechat.onechatsdk.services.datasource

import com.google.gson.JsonObject
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.response.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
interface ContactNetworkDataSourceInterface {
    @POST("/user/getuserInfo")
    fun onUserInfo(@Body body: JsonObject?): Call<UserInfoResponse>

    @POST("/user/addfriend")
    fun onAddFriend(@Body body: JsonObject?): Call<AcceptFriendResponse>

    @POST("/user/acceptfriend")
    fun onAcceptFriend(@Body body: JsonObject?): Call<AcceptFriendResponse>

    @POST("/user/cancelrequestfriend")
    fun onCancelRequestFriend(@Body body: JsonObject?): Call<BaseResponse>

    @POST("/user/cancelsendrequestfriend")
    fun onCancelSendRequestFriend(@Body body: JsonObject?): Call<BaseResponse>

    @POST("/user/getlistfriend")
    fun onGetListFriend(@Body body: JsonObject?): Call<GetListFriendResponse>

    @POST("/user/getlistrequestfriend")
    fun onGetListRequestFriend(@Body body: JsonObject?): Call<GetListRequestFriendResponse>

    @POST("/user/getlistpendingfriend")
    fun onGetListPendingFriend(@Body body: JsonObject?): Call<GetListRequestFriendResponse>

    @POST("/group/creategroup")
    fun onCreateGroup(@Body body: JsonObject?): Call<CreateGroupResponse>

    @POST("/group/getgroup")
    fun onGetGroup(@Body body: JsonObject?): Call<GetGroupResponse>

    @POST("/group/acceptinvitegroup")
    fun onAcceptInviteGroup (@Body body: JsonObject?): Call<GetGroupResponse>

    @POST("/group/setnamegroup")
    fun onSetNameGroup (@Body body: JsonObject?): Call<BaseResponse>

    @POST("/group/invitemembergroup")
    fun onInviteMemberGroup(@Body body: JsonObject?): Call<InviteMemberGroupResponse>

    @POST("/group/leavegroup")
    fun onLeaveGroup(@Body body: JsonObject?): Call<BaseResponse>

    @POST("/group/getlistgroup")
    fun onGetListGroup(@Body body: JsonObject?): Call<GetListGroupResponse>

    @POST("/group/getlistpendinggroup")
    fun onGetListPendingGroup(@Body body: JsonObject?): Call<GetListPendingGroupResponse>

    @POST("/user/getlistchat")
    fun onGetListChat(@Body body: JsonObject?): Call<ChatResponse>



}