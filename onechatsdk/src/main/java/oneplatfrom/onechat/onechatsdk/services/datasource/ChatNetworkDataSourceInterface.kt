package oneplatfrom.onechat.onechatsdk.services.datasource

import com.google.gson.JsonObject
import oneplatfrom.onechat.onechatsdk.model.contact.response.ChatResponse
import retrofit2.Call
import retrofit2.http.*

interface ChatNetworkDataSourceInterface {

    @POST("v2/getListChat")
    fun onGetRoom(@Body request: JsonObject?): Call<ChatResponse>
}