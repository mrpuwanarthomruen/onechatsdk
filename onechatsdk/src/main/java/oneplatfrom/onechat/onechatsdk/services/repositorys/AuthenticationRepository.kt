package oneplatfrom.onechat.onechatsdk.services.repositorys

import android.text.TextUtils
import oneplatfrom.onechat.onechatsdk.error.AppRemoteException
import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import oneplatfrom.onechat.onechatsdk.services.ServiceBase
import oneplatfrom.onechat.onechatsdk.services.common.RepositoryCallback
import oneplatfrom.onechat.onechatsdk.services.datasource.AuthenticationNetworkDataSource
import oneplatfrom.onechat.onechatsdk.services.datasource.AuthenticationNetworkDataSourceInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AuthenticationRepository {

    private var mDataSource: AuthenticationNetworkDataSource = AuthenticationNetworkDataSource()

    fun signIn(encodeLogin: String, callback: RepositoryCallback<AuthenticationResponse>) {
        mDataSource.signIn(encodeLogin).enqueue(object : Callback<AuthenticationResponse> {
            override fun onResponse(call: Call<AuthenticationResponse>?, response: Response<AuthenticationResponse>) {
                try {
                    if (response.isSuccessful) {
                        val dao = response.body() ?: throw AppRemoteException.parse(response)

                        if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                            callback.onSuccess(dao)
                        } else if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_FAIL)) {
                            if (dao.messageResponse.equals("Wrong Username / Password")) {
                                throw AppRemoteException("Invalid username or password.")
                            } else {
                                throw AppRemoteException("Could not connect to server. Please try again later")
                            }
                        } else {
                            throw AppRemoteException(dao.getFormattedResponseMessage())
                        }
                    } else {
                        throw AppRemoteException.parse(response)
                    }
                } catch (e: Exception) {
                    callback.onFailure(e)
                }
            }

            override fun onFailure(call: Call<AuthenticationResponse>?, t: Throwable?) {
                if (call?.isCanceled == false) {
                    callback.onFailure(t)
                }
            }
        })
    }
}