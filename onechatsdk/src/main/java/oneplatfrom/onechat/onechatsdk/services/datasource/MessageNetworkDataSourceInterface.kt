package oneplatfrom.onechat.onechatsdk.services.datasource

import com.google.gson.JsonObject
import okhttp3.RequestBody
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.message.Answer
import oneplatfrom.onechat.onechatsdk.model.message.AnswerMessageRequest
import oneplatfrom.onechat.onechatsdk.model.message.response.ListStickerMessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageListResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.QuickReplyMessageResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
interface MessageNetworkDataSourceInterface {
    @POST("/get_message")
    fun onGetMessage(@Body body: JsonObject?): Call<MessageResponse>

    @POST("/get_list_message")
    fun onGetMessageList(@Body body: JsonObject?): Call<MessageListResponse>

    @POST("/user/push_text_msg")
    fun onPushTextMessage(@Body body: JsonObject?): Call<MessageResponse>

    @POST("/user/push_location_msg")
    fun onPushLocationMessage(@Body body: JsonObject?): Call<MessageResponse>

    @POST("/get_quick_reply")
    fun onGetQuickReplyMessage(@Body body: JsonObject?): Call<QuickReplyMessageResponse>

    @POST("/disable_quick_reply")
    fun onGetDisableQuickReply(@Body body: JsonObject?): Call<BaseResponse>

    @POST("/user/push_sticker_msg")
    fun onPushStickerMessage(@Body body: JsonObject?): Call<MessageResponse>

    @POST("/user/get_list_sticker")
    fun onGetListStickerMessage(): Call<ListStickerMessageResponse>

    @POST("/user/push_file_msg")
    fun onPushFileMessage(@Body requestBody: RequestBody): Call<MessageResponse>

    @POST("/user/answer_quickreply")
    fun onAnswerMessage(@Body body: AnswerMessageRequest): Call<MessageResponse>


}