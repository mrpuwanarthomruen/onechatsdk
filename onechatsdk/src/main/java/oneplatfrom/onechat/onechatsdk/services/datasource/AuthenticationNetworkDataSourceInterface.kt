package oneplatfrom.onechat.onechatsdk.services.datasource

import com.google.gson.JsonObject
import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
interface AuthenticationNetworkDataSourceInterface {
    @POST("/user/loginusername")
    fun signIn(@Body body: JsonObject?): Call<AuthenticationResponse>
}