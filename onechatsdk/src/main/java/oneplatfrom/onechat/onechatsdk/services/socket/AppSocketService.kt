package oneplatfrom.onechat.onechatsdk.services.socket

import android.content.Context
import okhttp3.OkHttpClient
import java.io.IOException
import java.io.InputStream
import java.security.KeyManagementException
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.*

abstract class AppSocketService(private val context: Context) {

    @Throws(Exception::class)
    protected fun getCertificate(certificateAssetPath: String): AppSocketCertificate {
        val certificateFactory =
            CertificateFactory.getInstance("X.509")
        var inputStream: InputStream? = null
        inputStream = try {
            context.assets.open(certificateAssetPath)
        } catch (e: IOException) {
            throw IOException("getCertificate: Cannot get certificate file from $certificateAssetPath")
        }
        val certificate: Certificate
        certificate = try {
            certificateFactory.generateCertificate(inputStream)
        } finally {
            inputStream.close()
        }
        val keyStoreType = KeyStore.getDefaultType()
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        keyStore.setCertificateEntry("ca", certificate)
        val trustManagerAlgorithm =
            TrustManagerFactory.getDefaultAlgorithm()
        val trustManagerFactory =
            TrustManagerFactory.getInstance(trustManagerAlgorithm)
        trustManagerFactory.init(keyStore)
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, trustManagerFactory.trustManagers, null)
        return AppSocketCertificate(
            certificate, keyStore, trustManagerFactory.trustManagers
            , sslContext
        )
    }

    @get:Throws(
        NoSuchAlgorithmException::class,
        KeyManagementException::class
    )
    protected val client: OkHttpClient
        get() {
            val hostnameVerifier =
                HostnameVerifier { hostname, session -> true }
            val sslContext = SSLContext.getInstance("TLS")
            val trustManagers =
                arrayOf<TrustManager>(object : X509TrustManager {
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                }
                )
            sslContext.init(null, trustManagers, SecureRandom())
            return OkHttpClient.Builder()
                .hostnameVerifier(hostnameVerifier)
                .sslSocketFactory(
                    sslContext.socketFactory,
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate?> {
                            return arrayOfNulls(0)
                        }
                    })
                .build()
        }

}