package oneplatfrom.onechat.onechatsdk.services.datasource

import oneplatfrom.onechat.onechatsdk.model.contact.response.ChatResponse
import oneplatfrom.onechat.onechatsdk.services.ServiceBase
import oneplatfrom.onechat.onechatsdk.services.common.AppRequestBuilder
import retrofit2.Call

class ChatNetworkDataSource: ServiceBase<ChatNetworkDataSourceInterface>() {

     private  fun getInterface(): ChatNetworkDataSourceInterface {
         return chatService().create(ChatNetworkDataSourceInterface::class.java)
    }

    fun onGetRoom(tokenUser: String): Call<ChatResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)

        return getInterface().onGetRoom(builder.build())
    }

}