package oneplatfrom.onechat.onechatsdk.services.datasource

import android.text.TextUtils
import okhttp3.MultipartBody
import oneplatfrom.onechat.onechatsdk.connection.FileRequestBody
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import oneplatfrom.onechat.onechatsdk.model.message.AnswerMessageRequest
import oneplatfrom.onechat.onechatsdk.model.message.response.ListStickerMessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageListResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.QuickReplyMessageResponse
import oneplatfrom.onechat.onechatsdk.services.ServiceBase
import oneplatfrom.onechat.onechatsdk.services.common.AppRequestBuilder
import retrofit2.Call
import java.io.File

class MessageNetworkDataSource : ServiceBase<MessageNetworkDataSource>() {

    private fun getInterface(): MessageNetworkDataSourceInterface {
        return onMessage().create(MessageNetworkDataSourceInterface::class.java)
    }

    fun onGetMessage(tokenMessage: String, tokenUser: String): Call<MessageResponse> {
        val builder = AppRequestBuilder()
            .add("tokenmessage", tokenMessage)
            .add("tokenuser", tokenUser)

        return getInterface().onGetMessage(builder.build())
    }

    fun onGetMessageList(tokenUser: String, tokenRoom: String, timeIn: String): Call<MessageListResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("tokenroom", tokenRoom)
            .add("time_in", timeIn)

        return getInterface().onGetMessageList(builder.build())
    }

    fun onPushTextMessage(tokenUser: String, tokenRoom: String, message: String): Call<MessageResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("tokenroom", tokenRoom)
            .add("message", message)

        return getInterface().onPushTextMessage(builder.build())
    }

    fun onPushLocationMassage(tokenUser: String, tokenRoom: String, lat: String, long: String, address: String): Call<MessageResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("tokenroom", tokenRoom)
            .add("lat", lat)
            .add("long", long)
            .add("address", address)

        return getInterface().onPushLocationMessage(builder.build())
    }

    fun onGetQuickReplyMessage(tokenRoom: String, tokenUser: String): Call<QuickReplyMessageResponse> {
        val builder = AppRequestBuilder()
            .add("tokenroom", tokenRoom)
            .add("tokenuser", tokenUser)

        return getInterface().onGetQuickReplyMessage(builder.build())
    }

    fun onGetDisableQuickReplyMessage(tokenRoom: String, tokenUser: String): Call<BaseResponse> {
        val builder = AppRequestBuilder()
            .add("tokenroom", tokenRoom)
            .add("tokenuser", tokenUser)

        return getInterface().onGetDisableQuickReply(builder.build())
    }

    fun onPushStickerMessage(tokenRoom: String, tokenUser: String, message: String): Call<MessageResponse> {
        val builder = AppRequestBuilder()
            .add("tokenroom", tokenRoom)
            .add("tokenuser", tokenUser)
            .add("message", message)

        return getInterface().onPushStickerMessage(builder.build())
    }

    fun onGetListStickerMessage(): Call<ListStickerMessageResponse> {
        return getInterface().onGetListStickerMessage()
    }

    fun onPushFileMessage(tokenRoom: String, tokenUser: String, pathUri: String): Call<MessageResponse> {
        val builder = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("tokenuser", tokenUser)
            .addFormDataPart("tokenroom", tokenRoom)

        if (!TextUtils.isEmpty(pathUri)) {
            val file = File(pathUri)

            if (file.exists()) {
                builder.addFormDataPart("file", file.name, FileRequestBody(file, "content", null))
            }
        }

        return getInterface().onPushFileMessage(builder.build())
    }

    fun onPushAnswerMessage(request : AnswerMessageRequest): Call<MessageResponse> {

        return getInterface().onAnswerMessage(request)
    }
}