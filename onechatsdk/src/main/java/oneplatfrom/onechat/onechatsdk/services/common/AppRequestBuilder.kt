package oneplatfrom.onechat.onechatsdk.services.common

import com.google.gson.JsonArray
import com.google.gson.JsonObject

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
class AppRequestBuilder {
    private var jsonObject = JsonObject()

    fun add(
        key: String?,
        value: String?
    ): AppRequestBuilder {
        jsonObject?.addProperty(key, value)
        return this
    }

    fun add(
        key: String?,
        valueList: List<String?>
    ): AppRequestBuilder {
        val jsonArray = JsonArray()
        for (value in valueList) {
            jsonArray.add(value)
        }
        jsonObject?.add(key, jsonArray)
        return this
    }

    fun build(): JsonObject? {
        return jsonObject
    }


}

