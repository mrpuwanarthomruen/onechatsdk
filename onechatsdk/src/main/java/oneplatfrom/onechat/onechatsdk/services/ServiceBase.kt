package oneplatfrom.onechat.onechatsdk.services

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by puwanarT on 16/6/2020 AD
 * Copyright (c) One Centric Company Limited All rights reserved.
 */
open class ServiceBase<T> {
    companion object {
        var STATUS_SUCCESS = "success"
        var STATUS_FAIL = "fail"
    }

    //    var BASEMANAGE = "http://203.151.50.18:6677"
    //    var BASEMESSAGE = "http://203.151.50.15:7000/dev_api"
    //    var BASECHAT = "https://testchat.one.th/go_api/api/"

    var BASEMESSAGE = "https://chat-api.one.th/sdk_msg/api/"
    var BASEMANAGE = "https://chat-api.one.th/sdk_mng/"
    var BASECHAT = "https://chat-api.one.th/go_dev/api/"


    protected fun onAuthentication(): Retrofit {
        val builder =
            Retrofit.Builder()
                .baseUrl(BASEMANAGE)
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
        return builder.build()
    }

    protected fun onContact(): Retrofit {
        val builder =
            Retrofit.Builder()
                .baseUrl(BASEMANAGE)
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
        return builder.build()
    }

    protected fun onMessage(): Retrofit {
        val builder =
            Retrofit.Builder()
                .baseUrl(BASEMESSAGE)
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
        return builder.build()
    }

    protected open fun chatService(): Retrofit {
        val builder =
            Retrofit.Builder()
                .baseUrl(BASECHAT)
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
        return builder.build()
    }
}