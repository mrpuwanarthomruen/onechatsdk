package oneplatfrom.onechat.onechatsdk.services.common


interface RepositoryCallback<T> {
    fun onSuccess(result: T?)
    fun onFailure(t: Throwable?)
}