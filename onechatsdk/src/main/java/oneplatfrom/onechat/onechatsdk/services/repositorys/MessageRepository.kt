package oneplatfrom.onechat.onechatsdk.services.repositorys

import android.text.TextUtils
import oneplatfrom.onechat.onechatsdk.error.AppRemoteException
import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import oneplatfrom.onechat.onechatsdk.model.message.AnswerMessageRequest
import oneplatfrom.onechat.onechatsdk.model.message.response.ListStickerMessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageListResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.MessageResponse
import oneplatfrom.onechat.onechatsdk.model.message.response.QuickReplyMessageResponse
import oneplatfrom.onechat.onechatsdk.services.ServiceBase
import oneplatfrom.onechat.onechatsdk.services.common.RepositoryCallback
import oneplatfrom.onechat.onechatsdk.services.datasource.AuthenticationNetworkDataSource
import oneplatfrom.onechat.onechatsdk.services.datasource.AuthenticationNetworkDataSourceInterface
import oneplatfrom.onechat.onechatsdk.services.datasource.MessageNetworkDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MessageRepository {

    private var mDataSource: MessageNetworkDataSource = MessageNetworkDataSource()

    fun onGetMessage(
        tokenMessage: String,
        tokenUser: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        mDataSource.onGetMessage(tokenMessage, tokenUser)
            .enqueue(object : Callback<MessageResponse> {
                override fun onResponse(
                    call: Call<MessageResponse>?,
                    response: Response<MessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<MessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetMessageList(
        tokenUser: String,
        tokenRoom: String,
        timeIn: String,
        callback: RepositoryCallback<MessageListResponse>
    ) {
        mDataSource.onGetMessageList(tokenUser, tokenRoom, timeIn)
            .enqueue(object : Callback<MessageListResponse> {
                override fun onResponse(
                    call: Call<MessageListResponse>?,
                    response: Response<MessageListResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<MessageListResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onPushTextMessage(
        tokenUser: String,
        tokenRoom: String,
        message: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        mDataSource.onPushTextMessage(tokenUser, tokenRoom, message)
            .enqueue(object : Callback<MessageResponse> {
                override fun onResponse(
                    call: Call<MessageResponse>?,
                    response: Response<MessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<MessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onPushLocationMessage(
        tokenUser: String,
        tokenRoom: String,
        lat: String,
        long: String,
        address: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        mDataSource.onPushLocationMassage(tokenUser, tokenRoom, lat, long, address)
            .enqueue(object : Callback<MessageResponse> {
                override fun onResponse(
                    call: Call<MessageResponse>?,
                    response: Response<MessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<MessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetQuickReplyMessage(
        tokenRoom: String,
        tokenUser: String,
        callback: RepositoryCallback<QuickReplyMessageResponse>
    ) {
        mDataSource.onGetQuickReplyMessage(tokenRoom, tokenUser)
            .enqueue(object : Callback<QuickReplyMessageResponse> {
                override fun onResponse(
                    call: Call<QuickReplyMessageResponse>?,
                    response: Response<QuickReplyMessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<QuickReplyMessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetDisableQuickReply(
        tokenRoom: String,
        tokenUser: String,
        callback: RepositoryCallback<BaseResponse>
    ) {
        mDataSource.onGetDisableQuickReplyMessage(tokenRoom, tokenUser)
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>?,
                    response: Response<BaseResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onPushStickerMessage(
        tokenRoom: String,
        tokenUser: String,
        message: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        mDataSource.onPushStickerMessage(tokenRoom, tokenUser, message)
            .enqueue(object : Callback<MessageResponse> {
                override fun onResponse(
                    call: Call<MessageResponse>?,
                    response: Response<MessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<MessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onGetListStickerMessage(
        callback: RepositoryCallback<ListStickerMessageResponse>
    ) {
        mDataSource.onGetListStickerMessage()
            .enqueue(object : Callback<ListStickerMessageResponse> {
                override fun onResponse(
                    call: Call<ListStickerMessageResponse>?,
                    response: Response<ListStickerMessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<ListStickerMessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onPushFileMessage(
        tokenRoom: String, tokenUser: String, pathUri: String,
        callback: RepositoryCallback<MessageResponse>
    ) {
        mDataSource.onPushFileMessage(tokenRoom, tokenUser, pathUri)
            .enqueue(object : Callback<MessageResponse> {
                override fun onResponse(
                    call: Call<MessageResponse>?,
                    response: Response<MessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<MessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }

    fun onPushAnswerMessage(
       request: AnswerMessageRequest,
        callback: RepositoryCallback<MessageResponse>
    ) {
        mDataSource.onPushAnswerMessage(request)
            .enqueue(object : Callback<MessageResponse> {
                override fun onResponse(
                    call: Call<MessageResponse>?,
                    response: Response<MessageResponse>
                ) {
                    try {
                        if (response.isSuccessful) {
                            val dao = response.body() ?: throw AppRemoteException.parse(response)

                            if (TextUtils.equals(dao.statusResponse, ServiceBase.STATUS_SUCCESS)) {
                                callback.onSuccess(dao)
                            } else {
                                throw AppRemoteException(dao.getFormattedResponseMessage())
                            }
                        } else {
                            throw AppRemoteException.parse(response)
                        }
                    } catch (e: Exception) {
                        callback.onFailure(e)
                    }
                }

                override fun onFailure(call: Call<MessageResponse>?, t: Throwable?) {
                    if (call?.isCanceled == false) {
                        callback.onFailure(t)
                    }
                }
            })
    }
}