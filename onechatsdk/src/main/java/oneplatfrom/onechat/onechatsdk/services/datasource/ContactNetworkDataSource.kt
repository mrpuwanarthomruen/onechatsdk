package oneplatfrom.onechat.onechatsdk.services.datasource

import oneplatfrom.onechat.onechatsdk.model.BaseResponse
import oneplatfrom.onechat.onechatsdk.model.contact.response.*
import oneplatfrom.onechat.onechatsdk.services.ServiceBase
import oneplatfrom.onechat.onechatsdk.services.common.AppRequestBuilder
import retrofit2.Call

class ContactNetworkDataSource : ServiceBase<ContactNetworkDataSourceInterface>() {

    private fun getInterface(): ContactNetworkDataSourceInterface {
        return onContact().create(ContactNetworkDataSourceInterface::class.java)
    }

    fun onUserInfo(tokenuser: String): Call<UserInfoResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenuser)

        return getInterface().onUserInfo(builder.build())
    }

    fun onAddFriend(tokenFriend: String, tokenUser: String): Call<AcceptFriendResponse> {
        val builder = AppRequestBuilder()
            .add("tokenfriend", tokenFriend)
            .add("tokenuser", tokenUser)

        return getInterface().onAddFriend(builder.build())
    }

    fun onAcceptFriend(tokenFriend: String, tokenUser: String): Call<AcceptFriendResponse> {
        val builder = AppRequestBuilder()
            .add("tokenfriend", tokenFriend)
            .add("tokenuser", tokenUser)

        return getInterface().onAcceptFriend(builder.build())
    }

    fun onGetListFriend(tokenUser: String): Call<GetListFriendResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)

        return getInterface().onGetListFriend(builder.build())
    }

    fun onGetListRequestFriend(tokenUser: String): Call<GetListRequestFriendResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)

        return getInterface().onGetListRequestFriend(builder.build())
    }

    fun onGetListPendingFriend(tokenUser: String): Call<GetListRequestFriendResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)

        return getInterface().onGetListPendingFriend(builder.build())
    }

    fun onCancelRequestFriend(tokenFriend: String, tokenUser: String): Call<BaseResponse> {
        val builder = AppRequestBuilder()
            .add("tokenfriend", tokenFriend)
            .add("tokenuser", tokenUser)

        return getInterface().onCancelRequestFriend(builder.build())
    }

    fun onCancelSendRequestFriend(tokenFriend: String, tokenUser: String): Call<BaseResponse> {
        val builder = AppRequestBuilder()
            .add("tokenfriend", tokenFriend)
            .add("tokenuser", tokenUser)

        return getInterface().onCancelSendRequestFriend(builder.build())
    }

    fun onCreateGroup(tokenUser: String, groupName: String,listMember : ArrayList<String>): Call<CreateGroupResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("group_name", groupName)
            .add("listmember",listMember)

        return getInterface().onCreateGroup(builder.build())
    }

    fun onGetGroup(tokenUser: String, groupId: String): Call<GetGroupResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("group_id", groupId )

        return getInterface().onGetGroup(builder.build())
    }

    fun onInviteMemberGroup(tokenUser: String, groupId: String,listMember : ArrayList<String>): Call<InviteMemberGroupResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("group_id", groupId)
            .add("listmember",listMember)

        return getInterface().onInviteMemberGroup(builder.build())
    }

    fun onSetNameGroup(groupName: String, groupId: String): Call<BaseResponse> {
        val builder = AppRequestBuilder()
            .add("name", groupName)
            .add("group_id", groupId )

        return getInterface().onSetNameGroup(builder.build())
    }

    fun onAcceptInviteGroup(tokenUser: String, groupId: String): Call<GetGroupResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("group_id", groupId)

        return getInterface().onAcceptInviteGroup(builder.build())
    }

    fun onLeaveGroup(tokenUser: String, groupId: String,listMember : ArrayList<String>): Call<BaseResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)
            .add("group_id", groupId)
            .add("listmember",listMember)

        return getInterface().onLeaveGroup(builder.build())
    }

    fun onGetListGroup(tokenUser: String): Call<GetListGroupResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)

        return getInterface().onGetListGroup(builder.build())
    }

    fun onGetListPendingGroup(tokenUser: String): Call<GetListPendingGroupResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)

        return getInterface().onGetListPendingGroup(builder.build())
    }

    fun onGetListChat(tokenUser: String): Call<ChatResponse> {
        val builder = AppRequestBuilder()
            .add("tokenuser", tokenUser)

        return getInterface().onGetListChat(builder.build())
    }
}