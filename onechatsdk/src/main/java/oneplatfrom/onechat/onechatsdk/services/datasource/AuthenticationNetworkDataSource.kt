package oneplatfrom.onechat.onechatsdk.services.datasource

import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import oneplatfrom.onechat.onechatsdk.services.ServiceBase
import oneplatfrom.onechat.onechatsdk.services.common.AppRequestBuilder
import retrofit2.Call

class AuthenticationNetworkDataSource : ServiceBase<AuthenticationNetworkDataSourceInterface>() {

    private fun getInterface(): AuthenticationNetworkDataSourceInterface {
        return onAuthentication().create(AuthenticationNetworkDataSourceInterface::class.java)
    }

    fun signIn(encodeLogin: String): Call<AuthenticationResponse> {
        val builder = AppRequestBuilder()
            .add("source", encodeLogin)

        return getInterface().signIn(builder.build())
    }
}