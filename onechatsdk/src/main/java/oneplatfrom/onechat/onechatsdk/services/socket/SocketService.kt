package oneplatfrom.onechat.onechatsdk.services.socket

import android.content.Context
import android.os.Handler
import android.os.Looper
import io.socket.client.IO
import io.socket.client.Socket
import oneplatfrom.onechat.onechatsdk.model.socketmodel.*
import org.json.JSONObject

class SocketService(context: Context, tokenUser: String) : AppSocketService(context) {

    interface Listener {
        fun onReceivedMessage(message: MessageData)
        fun onAddFriend(friend: AddFriendData)
        fun onAcceptFriend(friend: AcceptFriendData)
        fun onCreateGroup(group: CreateGroupData)
        fun onGroupInvite(invite: GroupInviteData)
        fun onActiveGroup(active: ActiveGroupData)
        fun onLeaveGroup(leave : LeaveGroupData)
    }

    private lateinit var socket: Socket
    private var mListener: Listener? = null
    private var BASESOCKET = "http://chat-public.one.th:8998"
//    private var BASESOCKET = "http://203.151.50.15:8998"



    init {
        initial(tokenUser)
    }

    fun connect(listener: Listener) {
        mListener = listener
        socket.connect()
    }

    fun disconnect() {
        socket.disconnect()
    }

    private fun initial(tokenUser: String) {
        try {
            val options = IO.Options()
            options.forceNew = true
            options.reconnection = true
            options.secure = true

            options.callFactory = client
            options.webSocketFactory = client

            socket = IO.socket(BASESOCKET, options)
            socket.on(Socket.EVENT_CONNECT) {
                joinSocket(tokenUser)
            }
            socket.on(Socket.EVENT_ERROR) {
                if (it.isNotEmpty()) {
                }
            }
            socket.on(Socket.EVENT_CONNECT_ERROR) {
                if (it.isNotEmpty()) {
                }
            }

            // message
            socket.on("message") {
                if (it.isNotEmpty() && it[0] is JSONObject) {
                    val obj = it[0] as JSONObject
                    val message = MessageData().apply {
                        this.tokenMessage = obj.optString("tokenmessage", "")
                        this.tokenRoom = obj.optString("tokenroom", "")
                        this.tokenUser = obj.optString("tokenuser", "")
                    }

                    onMessage(message)
                }
            }

            // add friend
            socket.on("event_friend") {
                if (it.isNotEmpty() && it[0] is JSONObject) {
                    val obj = it[0] as JSONObject
                    val friend = AddFriendData().apply {
                        this.tokenUser = obj.optString("tokenuser", "")
                        this.tokenFriend = obj.optString("tokenfriend", "")
                    }

                    onAddFriend(friend)
                }
            }

            // accept friend
            socket.on("event_accept_friend") {
                if (it.isNotEmpty() && it[0] is JSONObject) {
                    val obj = it[0] as JSONObject
                    val friend = AcceptFriendData().apply {
                        this.tokenUser = obj.optString("tokenuser", "")
                        this.tokenFriend = obj.optString("tokenfriend", "")
                        this.tokenRoom = obj.optString("tokenroom", "")
                        this.timestamp = obj.optString("timestamp", "")
                    }

                    onAcceptFriend(friend)
                }
            }

            // create group
            socket.on("event_create_group") {
                if (it.isNotEmpty() && it[0] is JSONObject) {
                    val obj = it[0] as JSONObject
                    val group = CreateGroupData().apply {
                        val list = obj.optJSONArray("listmember")
                        val listUser = ArrayList<String>()
                        if (list.length() > 0) {
                            for (i in 0 until list.length()) {
                                listUser.add(list.getString(i))
                            }
                        }

                        this.listMember = listUser
                        this.groupId = obj.optString("groupid", "")
                        this.fromTokenUser = obj.optString("fromtokenuser", "")
                    }

                    onCreateGroup(group)
                }
            }

            // invite group
            socket.on("event_invite_group") {
                if (it.isNotEmpty() && it[0] is JSONObject) {
                    val obj = it[0] as JSONObject
                    val group = GroupInviteData().apply {
                        val listMember = obj.optJSONArray("memberingroup")
                        val listInvited = obj.optJSONArray("invited")

                        val listMembers = ArrayList<String>()
                        val listInvites = ArrayList<String>()

                        if (listMember.length() > 0) {
                            for (i in 0 until listMember.length()) {
                                listMembers.add(listMember.getString(i))
                            }
                        }

                        if (listInvited.length() > 0) {
                            for (i in 0 until listInvited.length()) {
                                listInvites.add(listInvited.getString(i))
                            }
                        }
                        this.tokenUser = obj.optString("tokenuser", "")
                        this.tokenRoom = obj.optString("tokenroom", "")
                        this.memberInGroup = listMembers
                        this.invited = listInvites
                        this.groupId = obj.optString("groupid", "")
                        this.message = obj.optString("message", "")
                    }

                    onGroupInvite(group)
                }
            }


            socket.on("event_active_group") {
                if (it.isNotEmpty() && it[0] is JSONObject) {
                    val obj = it[0] as JSONObject
                    val group = ActiveGroupData().apply {

                        this.tokenUser = obj.optString("tokenuser", "")
                        this.groupId = obj.optString("groupid", "")
                        this.timestamp = obj.optString("timestamp", "")
                    }

                    onActiveGroup(group)
                }
            }

            socket.on("event_leave_group") {
                if (it.isNotEmpty() && it[0] is JSONObject) {
                    val obj = it[0] as JSONObject
                    val group = LeaveGroupData().apply {
                        val listMember = obj.optJSONArray("listmember")
                        val listKick = obj.optJSONArray("listkick")

                        val listMembers = ArrayList<String>()
                        val listKicks = ArrayList<String>()

                        if (listMember.length() > 0) {
                            for (i in 0 until listMember.length()) {
                                listMembers.add(listMember.getString(i))
                            }
                        }

                        if (listKick.length() > 0) {
                            for (i in 0 until listKick.length()) {
                                listKicks.add(listKick.getString(i))
                            }
                        }
                        this.listmember = listMembers
                        this.listKicker = listKicks
                        this.tokenUser = obj.optString("tokenuser", "")
                        this.tokenRoom = obj.optString("tokenroom", "")
                        this.message = obj.optString("message", "")
                    }

                    onLeaveGroup(group)
                }
            }


        } catch (e: Exception) {
        }
    }


    private fun joinSocket(tokenRoom: String) {
        val obj = JSONObject()
        obj.put("room", tokenRoom)

        socket.emit("join", obj)
    }

    private fun onMessage(message: MessageData) {
        Handler(Looper.getMainLooper()).post {
            mListener?.onReceivedMessage(message)
        }
    }

    private fun onAddFriend(friend: AddFriendData) {
        Handler(Looper.getMainLooper()).post {
            mListener?.onAddFriend(friend)
        }
    }

    private fun onAcceptFriend(friend: AcceptFriendData) {
        Handler(Looper.getMainLooper()).post {
            mListener?.onAcceptFriend(friend)
        }
    }

    private fun onCreateGroup(group: CreateGroupData) {
        Handler(Looper.getMainLooper()).post {
            mListener?.onCreateGroup(group)
        }
    }

    private fun onGroupInvite(invite: GroupInviteData) {
        Handler(Looper.getMainLooper()).post {
            mListener?.onGroupInvite(invite)
        }
    }

    private fun onActiveGroup(active: ActiveGroupData) {
        Handler(Looper.getMainLooper()).post {
            mListener?.onActiveGroup(active)
        }
    }

    private fun onLeaveGroup(leave : LeaveGroupData){
        Handler(Looper.getMainLooper()).post {
            mListener?.onLeaveGroup(leave)
        }
    }

}