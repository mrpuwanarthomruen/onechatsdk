package oneplatfrom.onechat.onechatsdk.connection;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

/**
 * Created by Melody Dev(trophy.tot@gmail.com) on June 04, 2015.
 * Copyright (c) 2557 A-Gape Consulting (Thailand) Co.Ltd.,. All rights reserved.
 */
public class FileRequestBody extends RequestBody {

    private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE

    private final File file;
    private final ProgressListener listener;
    private final String contentType;

    public FileRequestBody(@NonNull File file, String contentType, @Nullable ProgressListener listener) {
        this.file = file;
        this.contentType = !TextUtils.isEmpty(contentType) ? contentType : "";
        this.listener = listener;
    }

    @Override
    public long contentLength() {
        return file.length();
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse(contentType);
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {
            source = Okio.source(file);
            long total = 0;
            long read;

            while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                total += read;
                sink.flush();
//                sink.emit();

                if (this.listener != null) {
                    this.listener.transferred(total);
                }
            }
        } finally {
            Util.closeQuietly(source);
        }
    }

    public interface ProgressListener {
        void transferred(long num);
    }
}
