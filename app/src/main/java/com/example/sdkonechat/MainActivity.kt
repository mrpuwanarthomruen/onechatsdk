package com.example.sdkonechat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import oneplatfrom.onechat.onechatsdk.Authentication
import oneplatfrom.onechat.onechatsdk.Contact
import oneplatfrom.onechat.onechatsdk.ManageCenter
import oneplatfrom.onechat.onechatsdk.model.authentication.response.AuthenticationResponse
import oneplatfrom.onechat.onechatsdk.model.contact.response.UserInfoResponse
import oneplatfrom.onechat.onechatsdk.services.common.RepositoryCallback
import oneplatfrom.onechat.onechatsdk.services.socket.SocketService

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

